<?php

use Illuminate\Database\Seeder;

class AnunciosTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('anuncios')->delete();
        
        \DB::table('anuncios')->insert(array (
            0 => 
            array (
                'id' => 1,
                'id_usuario' => 1,
                'id_livro' => 1,
                'nome' => 'Livro de Matemática 8ª série',
                'preco' => 35,
                'estado' => 'Usado',
                'imagem' => NULL,
                'descricao' => 'Livro faltando algumas paginas mas bem encapado',
                'created_at' => '2018-07-10 15:21:42',
                'updated_at' => '2018-07-11 14:32:22',
            ),
            1 => 
            array (
                'id' => 2,
                'id_usuario' => 1,
                'id_livro' => 2,
                'nome' => 'Livro de Portugues para Crianças',
                'preco' => 12,
                'estado' => 'Novo',
                'imagem' => NULL,
                'descricao' => 'Livro bem conservado.',
                'created_at' => '2018-07-10 15:21:42',
                'updated_at' => '2018-07-11 14:32:22',
            ),
            2 => 
            array (
                'id' => 3,
                'id_usuario' => 1,
                'id_livro' => 4,
                'nome' => 'Livro Vidas Secas - (Grande Oferta)',
                'preco' => 23,
                'estado' => 'Novo',
                'imagem' => NULL,
                'descricao' => 'Excelente livro para leitura noturna',
                'created_at' => '2018-07-10 15:21:42',
                'updated_at' => '2018-07-11 14:32:22',
            ),
        ));
        
        
    }
}