<?php

use Illuminate\Database\Seeder;

class LivrosTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('livros')->delete();
        
        \DB::table('livros')->insert(array (
            0 => 
            array (
                'id' => 1,
                'id_usuario' => 1,
                'titulo' => 'Matematica Fundamental',
                'autor' => 'Carlos Felipe',
                'materia' => 'matematica',
                'edicao' => '1',
                'tipo' => 'didatico',
                'created_at' => '2018-07-10 15:21:21',
                'updated_at' => '2018-07-10 15:21:21',
            ),
            1 => 
            array (
                'id' => 2,
                'id_usuario' => 1,
                'titulo' => 'Português Básico',
                'autor' => 'Noah Silva',
                'materia' => 'portugues',
                'edicao' => '1',
                'tipo' => 'didatico',
                'created_at' => '2018-07-11 14:34:00',
                'updated_at' => '2018-07-11 14:34:00',
            ),
            2 => 
            array (
                'id' => 3,
                'id_usuario' => 1,
                'titulo' => 'Literatura Brasileira',
                'autor' => 'Paulo Cesar',
                'materia' => 'literatura',
                'edicao' => '2',
                'tipo' => 'didatico',
                'created_at' => '2018-07-11 14:34:22',
                'updated_at' => '2018-07-11 14:34:22',
            ),
            3 => 
            array (
                'id' => 4,
                'id_usuario' => 1,
                'titulo' => 'Vidas Secas',
                'autor' => 'Graciliano Ramos',
                'materia' => NULL,
                'edicao' => '1',
                'tipo' => 'paradidatico',
                'created_at' => '2018-07-11 14:34:55',
                'updated_at' => '2018-07-11 14:34:55',
            ),
            
        ));
        
        
    }
}