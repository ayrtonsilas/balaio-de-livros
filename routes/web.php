<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [
    'uses' => 'HomeController@index',
]);

Route::get('/didaticos/{materia}', 'HomeController@didaticos');
Route::get('/paradidaticos', 'HomeController@paradidaticos');

Auth::routes();

/*home*/
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/detalhes/{anuncio}', 'HomeController@detalhes')->name('home');
Route::post('/pesquisar', 'HomeController@pesquisar');
Route::post('/salvar', 'InteresseController@salvar');


/*livros*/
Route::prefix('livros')->group(function () {
    Route::get('/', [
        'uses' => 'LivroController@listar',
    ]);
    Route::get('/novo', [
        'uses' => 'LivroController@novo',
    ]);
    Route::get('/editar/{livro}', [
        'uses' => 'LivroController@editar',
    ]);
    Route::post('/salvar', [
        'uses' => 'LivroController@salvar',
    ]);
    Route::delete('/excluir/{livro}', [
        'uses' => 'LivroController@excluir',
    ]);
});
/*anuncios*/
Route::prefix('anuncios')->group(function () {
    Route::get('/', [
        'uses' => 'AnuncioController@listar',
    ]);
    Route::get('/novo', [
        'uses' => 'AnuncioController@novo',
    ]);
    Route::get('/editar/{anuncio}', [
        'uses' => 'AnuncioController@editar',
    ]);
    Route::post('/salvar', [
        'uses' => 'AnuncioController@salvar',
    ]);
    Route::delete('/excluir/{anuncio}', [
        'uses' => 'AnuncioController@excluir',
    ]);
});

/*interesses*/
Route::prefix('interesses')->group(function () {
    Route::get('/', [
        'uses' => 'InteresseController@listar',
    ]);
    Route::delete('/excluir/{interesse}', [
        'uses' => 'InteresseController@excluir',
    ]);
    Route::post('/concluir', [
        'uses' => 'InteresseController@concluir',
    ]);
});

/*anuncios*/
Route::prefix('meusdesejos')->group(function () {
    Route::get('/', [
        'uses' => 'MeusDesejosController@listar',
    ]);
    Route::delete('/desistir/{interesse}', [
        'uses' => 'MeusDesejosController@desistir',
    ]);
});

/*perfil*/
Route::prefix('perfil')->group(function () {
    Route::get('/', [
        'uses' => 'OperadorController@form',
    ]);
    Route::post('/salvar', [
        'uses' => 'OperadorController@salvar',
    ]);
});

