//instalar o php

sudo apt-get update
sudo add-apt-repository ppa:ondrej/php
sudo apt-get update
sudo apt-get install php7.1
sudo apt-get install php7.1-cli php7.1-common php7.1-json php7.1-opcache php7.1-mysql php7.1-mbstring php7.1-mcrypt php7.1-zip php7.1-fpm
sudo a2dismod php7.2
sudo a2enmod php7.1
sudo service apache2 restart


//instalar o composer

sudo apt-get update
sudo apt-get install curl
sudo curl -s https://getcomposer.org/installer | php
sudo mv composer.phar /usr/local/bin/composer
composer global require "laravel/installer"
$HOME/.config/composer/vendor/bin

// preparando o ambiente para execução
	- criar um arquivo chamado .env
	- Copiar todo o conteúdo do arquivo .env.example e colar no arquivo .env criado
	- rodar comando no terminal: php artisan key:generate
	- copiar a key e colar depois do "=" na variavel APP_KEY dentro do arquivo .env

// Instalar Dependencias do Projeto
 - composer install --no-scripts

// Rodar projeto
 - php artisan serve

//se for um projeto novo não precisa executar a linha de cima, basta fazer todos os anteriores
//abrir a pasta do projeto no terminar e rodar

composer update
