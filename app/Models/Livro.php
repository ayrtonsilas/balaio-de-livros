<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Livro extends Model
{

    protected $table = 'livros';
    protected $primaryKey = 'id';
    protected $guarded = ['id', 'created_at', 'update_at'];
    protected $fillable = [
        'id_usuario', 'autor', 'materia', 'edicao','titulo', 'tipo'
    ];
    
    public static function forSelect(){
        $livros = Livro::all();
        $select = [];
        foreach ($livros as $livro) {
            $select[$livro->id] = $livro->titulo;
        }
        
        return $select;
    }
    
    public function anuncios(){
        return $this->hasMany('App\Models\Anuncio', 'id_livro');
    }
    public function user(){
        return $this->belongsTo('App\Models\User');
    }
}
