<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Anuncio extends Model
{
    protected $table = 'anuncios';
    protected $primaryKey = 'id';
    protected $guarded = ['id', 'created_at', 'update_at'];
    protected $fillable = [
        'id_usuario', 'id_livro', 'nome','preco','estado','imagem','descricao'
    ];
    public static function tipo($materia = false){

        if (!$materia){
            $result = Anuncio::join('livros', 'livros.id', 'anuncios.id_livro')->where('livros.tipo', 'paradidatico')->select('anuncios.*','livros.autor', 'livros.materia', 'livros.edicao','livros.titulo')->get();
        }
        else{
            $result = Anuncio::join('livros', 'livros.id', 'anuncios.id_livro')->where('livros.materia', $materia)->select('anuncios.*','livros.autor', 'livros.materia', 'livros.edicao','livros.titulo')->get();
        }
        return $result;
    }
    public function user(){
        return $this->belongsTo('App\Models\User', 'id_usuario');
    }
    public function livro(){
        return $this->belongsTo('App\Models\Livro', 'id_livro');
    }
    public function interesses(){
        return $this->hasMany('App\Models\Interesse','id_anuncio');
    }
}
