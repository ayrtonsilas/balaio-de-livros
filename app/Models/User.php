<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'data_nasc', 'telefone'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    public function anuncios(){
        return $this->hasMany('App\Models\Anuncio', 'id_usuario');
    }
    public function desejados(){
        return $this->hasMany('App\Models\Desejado');
    }
    public function livros(){
        return $this->hasMany('App\Models\Desejado');
    }
    public function feedbacks(){
        return $this->hasMany('App\Models\Feedback');
    }
}
