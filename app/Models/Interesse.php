<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Interesse extends Model
{
    protected $table = 'interesses';
    protected $primaryKey = 'id';
    protected $guarded = ['id', 'created_at', 'update_at'];
    protected $fillable = [
        'id_usuario', 'id_anuncio','status'
    ];
    
    public function user(){
        return $this->belongsTo('App\Models\User', 'id_usuario');
    }
    public function anuncio(){
        return $this->belongsTo('App\Models\Anuncio','id_anuncio');
    }
    public static function getInteressesUser(){
        if(Auth::user() != null){
            $idUser = Auth::user()->id;
            return $interesses = Interesse::
                join('anuncios','anuncios.id','=','interesses.id_anuncio')->
                join('users','users.id','=','interesses.id_usuario')->
                where('anuncios.id_usuario',$idUser)->
                select('anuncios.nome as NOMEANUNCIO','users.name as NOMEINTERESSADO', 
                'users.email as EMAILINTERESSADO', 
                'users.telefone as TELEFONEINTERESSADO','interesses.status','interesses.id')->get();
        }else{
            return 0;
        }
    }
    public static function getInteressesUserNotificacao(){
        if(Auth::user() != null){
            $idUser = Auth::user()->id;
            return $interesses = Interesse::
                join('anuncios','anuncios.id','=','interesses.id_anuncio')->
                join('users','users.id','=','interesses.id_usuario')->
                where('anuncios.id_usuario',$idUser)->
                where('interesses.status','<>','Concluido')->
                select('anuncios.nome as NOMEANUNCIO','users.name as NOMEINTERESSADO', 
                'users.email as EMAILINTERESSADO', 
                'users.telefone as TELEFONEINTERESSADO','interesses.status','interesses.id')->get();
        }else{
            return 0;
        }
    }
}
