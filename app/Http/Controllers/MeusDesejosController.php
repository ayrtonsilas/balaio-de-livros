<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Models\{Livro,Interesse};
use Illuminate\Support\Facades\{DB, Input};

class MeusDesejosController extends Controller
{
    //mostra os livros
    public function listar(){
        $livros = Interesse::where('interesses.id_usuario','=',Auth::user()->id)->
        join('anuncios','anuncios.id','=','interesses.id_anuncio')->
        join('users','users.id','=','anuncios.id_usuario')->
        join('livros','livros.id','=','anuncios.id_livro')->select('livros.*','users.name as publicadopor', 'interesses.id as idinteresse','interesses.status')->get();

        return view('meusdesejos.listar',
        [
            'livros' => $livros,
        ]);
    }
    public function desistir(Interesse $interesse){
        try{
            $interesse->delete();
            return redirect()->action('MeusDesejosController@listar')
                ->with('success', 'Interesse excluido com sucesso!');
        }catch(\Exception $e){
            return redirect()->action('MeusDesejosController@listar')
                ->with('error', 'Não foi possível excluir interesse')
                ->withInput(Input::all());
        }
    }
    
}