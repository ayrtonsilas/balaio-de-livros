<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Models\Anuncio;
use App\Models\Livro;
use Illuminate\Support\Facades\{DB, Input};

class AnuncioController extends Controller
{
    public function listar(){
        return view('anuncios.listar',
        [
            'mensagem' => [],
        ]);
    }
    //monta um novo anuncio
    public function novo(){
        $anuncio = New Anuncio();
        $anuncio->id = 0;
        return self::editar($anuncio);
    }
    //editar um anuncio parassando o id do anuncio por parametro e o laravel ja converte pra objeto
    public function editar(Anuncio $anuncio){
        if(empty($anuncio->id)){
            $acao = 'novo';
        }else{
            $acao = 'editar';
        }
        
        $livros = Livro::forSelect();

        return view('anuncios.form',
        [
            'anuncio' => $anuncio,
            'acao'  => $acao,
            'livros' => $livros,
        ]);
    }

    public function excluir(Anuncio $anuncio){
        try{
            $anuncio->delete();
            return redirect()->action('AnuncioController@listar')
                ->with('success', 'Anuncio Excluido com sucesso!');
        }catch(\Exception $e){
            return redirect()->action('AnuncioController@listar')
                ->with('error', 'Não foi possível excluir')
                ->withInput(Input::all());
        }
    }
    public function salvar(Request $req){
        try{
            /*if(!empty($req->id_livro)){
                $livro = Anuncio::where('id_livro','=',$req->id_livro)->get();
                if(count($livro) > 0 && $livro->first()->id != $req->id){
                    return redirect()->action('AnuncioController@listar')
                    ->with('error', 'Livro já cadastrado em uma Anúncio')
                    ->withInput(Input::all());
                }
            }*/
            $anuncio = Anuncio::findOrNew($req->id);
            $anuncio->fill($req->all());
            $anuncio->id_usuario = Auth::user()->id;
            
            if (!empty($req->imagem)) {
                $this->validate($req, [
                    'imagem' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                ]);
                    
                //salvar Imagem
                $image = $req->file('imagem');
                $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('/images');
                $image->move($destinationPath, $input['imagename']);
                $anuncio->imagem = $input['imagename'];
            }
            $anuncio->save();
            return redirect()->action('AnuncioController@listar')
            ->with('success', 'Anuncio Salvo com sucesso!');
            
        }catch(\Exception $e){
            return redirect()->action('AnuncioController@listar')
                ->with('error', 'Não foi possível salvar')
                ->withInput(Input::all());
        }

    }
}