<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Models\{Livro,Interesse};
use Illuminate\Support\Facades\{DB, Input};
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class OperadorController extends Controller
{
    //mostra os livros
    public function form(){
        $operador = Auth::user();
        return view('perfil.form',
        [
            'operador' => $operador,
        ]);
    }
    public function salvar(Request $req){
        try{
            $usuario = User::find($req->id);
            if(!empty($req->password)){
                $usuario->password = Hash::make($req->password);
            }
            if(!empty($req->telefone)){
                $usuario->telefone = $req->telefone;
            }
            $usuario->save();
            return redirect()->action('OperadorController@form')
                ->with('success', 'Usuário editado com sucesso!');
        }catch(\Exception $e){
            return redirect()->action('OperadorController@form')
                ->with('error', 'Não foi possível editar o usuário')
                ->withInput(Input::all());
        }
        return view('perfil.form',
        [
            'operador' => $operador,
        ]);
    }
    
}