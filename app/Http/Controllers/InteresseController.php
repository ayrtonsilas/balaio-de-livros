<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Interesse;
use Auth;

class InteresseController extends Controller
{
    public function salvar(Request $request){
        try{
            $interesse = New Interesse();
            $interesse->fill($request->all());
            $interesse->status = 'Aguardando';
            
            $interesse->save();
            return redirect()->action('HomeController@index')
                ->with('success', 'Interesse cadastrado com sucesso!');
        }catch(\Exception $e){
            return redirect()->action('HomeController@index')
                ->with('error', 'Não foi possível salvar!');
        }
    }
    public function listar(){
        $interesses = Interesse::getInteressesUser();
        return View('interesses.listar',['interesses' => $interesses]);
    }
    public function excluir(Interesse $interesse){
        try{
            $interesse->delete();
            return redirect()->action('InteresseController@listar')
                ->with('success', 'Interesse excluido com sucesso!');
        }catch(\Exception $e){
            return redirect()->action('InteresseController@listar')
                ->with('error', 'Não foi possível excluir interesse')
                ->withInput(Input::all());
        }
    }
    public function concluir(Request $req){
        try{
            $interesse = Interesse::find($req->id);
            $interesse->status = 'Concluido';
            $interesse->save();
            return redirect()->action('InteresseController@listar')
                ->with('success', 'Interesse Finalizado com sucesso!');
        }catch(\Exception $e){
            return redirect()->action('InteresseController@listar')
                ->with('error', 'Não foi possível Finalizar Interesse')
                ->withInput(Input::all());
        }
    }
}
