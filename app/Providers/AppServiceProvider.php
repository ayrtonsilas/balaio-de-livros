<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use App\Models\Interesse;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
            view()->composer('layouts.app', function($view)
            {
                if(!empty(Interesse::getInteressesUserNotificacao())){
                    $view->with('countInteresses', count(Interesse::getInteressesUserNotificacao()));
                }else{
                    $view->with('countInteresses', 0);
                }
            });
        
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
