<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Models\Livro;
use Illuminate\Support\Facades\{DB, Input};

class LivroController extends Controller
{
    //mostra os livros
    public function listar(){
        $livros = Livro::where('id_usuario','=',Auth::user()->id)->get();

        return view('livros.listar',
        [
            'livros' => $livros,
            'mensagem' => [],
        ]);
    }
    //monta um novo livro
    public function novo(){
        $livro = New Livro();
        $livro->id = 0;
        return self::editar($livro);
    }
    //editar um livro parassando o id do livro por parametro e o laravel ja converte pra objeto
    public function editar(Livro $livro){
        if(empty($livro->id)){
            $acao = 'novo';
        }else{
            $acao = 'editar';
        }

        return view('livros.form',
        [
            'livro' => $livro,
            'acao'  => $acao
        ]);
    }

    public function excluir(Livro $livro){
        try{
            $livro->delete();
            return redirect()->action('LivroController@listar')
                ->with('success', 'Livro Excluido com sucesso!');
        }catch(\Exception $e){
            return redirect()->action('LivroController@listar')
                ->with('error', 'Não foi possível excluir')
                ->withInput(Input::all());
        }
    }
    public function salvar(Request $req){
        try{
            $livros = Livro::where('titulo',$req->titulo)->where('autor',$req->autor)->where('edicao',$req->edicao)->get();
            if(count($livros) > 0){
                return redirect()->action('LivroController@listar')
                ->with('error', 'Já existe esse livro cadastrado')
                ->withInput(Input::all());
            }else{
                $livro = Livro::findOrNew($req->id);
                $livro->fill($req->all());
                $livro->id_usuario = Auth::user()->id;
                $livro->save();
                return redirect()->action('LivroController@listar')
                    ->with('success', 'Livro Salvo com sucesso!');
             }

        }catch(\Exception $e){
            return redirect()->action('LivroController@listar')
                ->with('error', 'Não foi possível salvar')
                ->withInput(Input::all());
        }

    }
}