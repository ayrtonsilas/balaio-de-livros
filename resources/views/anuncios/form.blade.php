@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center text-center">
        
        <div class="col-sm-6">
            <h1>Anuncio</h1>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-sm-6">
            <form method="post" action="{{ action('AnuncioController@salvar') }}" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" name="id" value="{{old('id',$anuncio->id)}}">
            <div class="form-group">
                    <label for="nome">Titulo do Anúncio:</label>
                    <input required name="nome" type="text" class="form-control" id="nome" value="{{old('nome',$anuncio->nome)}}">
                </div>
                <div class="form-group">
                    <label for="descricao">Descrição:</label>
                    <textarea required name="descricao" class="form-control" id="descricao" placeholder="Fale sobre o livro que está disponível, inclua telefone para contato.">{{old('descricao',$anuncio->descricao)}}</textarea>
                </div>
                <div class="form-group">
                    <label for="id_livro">Livro:</label>
                    <select required class="form-control" name="id_livro" id="id_livro">
                        <option value="">Selecione um livro</option>
                        @foreach($livros as $k => $livro)
                            <option @if($k == $anuncio->id_livro) selected  @endif value="{{$k}}">{{$livro}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="estado">Estado do livro:</label>
                    <input required name="estado" type="text" class="form-control" id="estado" value="{{old('estado',$anuncio->estado)}}">
                </div>
                <div class="form-group">
                    <label for="preco">Preço:</label>
                    R$<input required name="preco" type="number" step="0.01" class="form-control" id="preco" value="{{old('preco',$anuncio->preco)}}" >
                </div>
                <div class="form-group">
                    <label for="imagem">Imagem:</label>
                    @if(!empty($anuncio->imagem))
                    <br><img class="col-sm-4" src="{{ URL::to('/images/' . $anuncio->imagem) }}"><br><br>
                    @endif
                    <input name="imagem" type="file" class="form-control" id="imagem" value="" >
                </div>
                <a href="{{ URL::previous() }}" class="btn btn-primary">Voltar</a>
                <button type="submit" class="btn btn-success">Salvar</button>
                @if($acao != "novo")
                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal">
                    Excluir
                </button>
                @endif
                
            </form>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            Tem certeza que deseja excluir o anuncio <b>{{$anuncio->nome}}</b>?
        </div>
        <div class="modal-footer">
            <form action="{{ action('AnuncioController@excluir',['anuncio' => $anuncio->id]) }}" method="POST">
                {{ method_field('DELETE') }}
                {{ csrf_field() }}
                <button type="button" class="btn btn-primary" data-dismiss="modal">Cancelar</button>
                <button class="btn btn-danger" type="submit" data-toggle="modal" data-target="#confirmDelete">
                    Excluir
                </button>
            </form>
        </div>
        </div>
    </div>
</div>
@endsection