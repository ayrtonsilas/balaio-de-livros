@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center text-center">
        
        <div class="col-sm-6">
            <h1>Anuncio</h1>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-sm-12">
            
            @if (\Session::has('success'))
                <div class="alert alert-success alert-dismissible fade show">
                    <ul>
                        <li>{!! \Session::get('success') !!}</li>
                    </ul>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            @if (\Session::has('error'))
                <div class="alert alert-danger alert-dismissible fade show">
                    <ul>
                        <li>{!! \Session::get('error') !!}</li>
                    </ul>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            
            <a href="{{action('AnuncioController@novo')}}" class="btn btn-success">Novo</a><br><br>
            <table style="border:1px solid #000;" class="table table-striped">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Titulo do Anúncio</th>
                        <th scope="col">Descrição</th>
                        <th scope="col">Livro</th>
                        <th scope="col">Estado do livro</th>
                        <th scope="col">Preço</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach(Auth::user()->anuncios as $anuncio)
                    <tr>
                        <th scope="row">
                            <a href="{{ action('AnuncioController@editar',['anuncio' => $anuncio->id]) }}">
                            {{$anuncio->id}}
                            </a>
                        </th>
                        <td>
                            <a href="{{ action('AnuncioController@editar',['anuncio' => $anuncio->id]) }}">
                            {{$anuncio->nome}}
                            </a>
                        </td>
                        <td>
                            <a href="{{ action('AnuncioController@editar',['anuncio' => $anuncio->id]) }}">
                            {{$anuncio->descricao}}
                            </a>
                        </td>
                        <td>
                            <a href="{{ action('AnuncioController@editar',['anuncio' => $anuncio->id]) }}">
                            {{$anuncio->titulo}}
                            </a>
                        </td>
                        <td>
                            <a href="{{ action('AnuncioController@editar',['anuncio' => $anuncio->id]) }}">
                            {{$anuncio->estado}}
                            </a>
                        </td>
                        <td>
                            <a href="{{ action('AnuncioController@editar',['anuncio' => $anuncio->id]) }}">
                            {{$anuncio->preco}}
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection