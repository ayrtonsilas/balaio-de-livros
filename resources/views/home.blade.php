@extends('layouts.app')

@section('content')
    <!-- Page Content -->
    <div class="container">

      <div class="row">

        <div class="col-lg-3">
          <div class="list-group">
            <a href="/paradidaticos" class="list-group-item">Paradidático</a>
            <a href="#collapseExample" class="list-group-item" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="collapseExample">Didático</a>
            <span class="collapse" id="collapseExample">
              <div class="list-group">
                <a href="/didaticos/portugues" class="list-group-item"><i class="fas fa-fw fa-arrow-circle-right"></i>Português</a>
                <a href="/didaticos/matematica" class="list-group-item"><i class="fas fa-fw fa-arrow-circle-right"></i>Matemática</a>
                <a href="/didaticos/literatura" class="list-group-item"><i class="fas fa-fw fa-arrow-circle-right"></i>Literatura</a>
                <a href="/didaticos/fisica" class="list-group-item"><i class="fas fa-fw fa-arrow-circle-right"></i>Física</a>
                <a href="/didaticos/quimica" class="list-group-item"><i class="fas fa-fw fa-arrow-circle-right"></i>Química</a>
                <a href="/didaticos/historia" class="list-group-item"><i class="fas fa-fw fa-arrow-circle-right"></i>História</a>
                <a href="/didaticos/geografia" class="list-group-item"><i class="fas fa-fw fa-arrow-circle-right"></i>Geografia</a>
              </div>
            </span>
          </div>
          <div class="row">
            <div class="col-sm-12">
            </div>
          </div>
          <br>
          <form action="{{ action('HomeController@pesquisar') }}" method="POST">
            <div class="form-group">
            {{ csrf_field() }}
              <label for="buscar">Busque por autor, matéria ou título</label>
              <input id="buscar" placeholder="Buscar no site..." class="form-control" type="search" name="buscar">
            </div>
            <input type="submit" value="Buscar" class="btn btn-success">
          </form>
        </div>
        <!-- /.col-lg-3 -->
        
        <div class="col-lg-9">
            @if (\Session::has('success'))
                <div class="alert alert-success alert-dismissible fade show">
                    <ul>
                        <li>{!! \Session::get('success') !!}</li>
                    </ul>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            @if (\Session::has('error'))
                <div class="alert alert-danger alert-dismissible fade show">
                    <ul>
                        <li>{!! \Session::get('error') !!}</li>
                    </ul>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <div class="row">
                @foreach($anuncios as $anuncio)
                <div class="col-lg-4 col-md-6 mb-4">
                <div class="card h-100">
                    <a href="{{ action('HomeController@detalhes',['anuncio' => $anuncio->id]) }}">
                      <img class="card-img-top" width="250" height="300" src="{{ asset($anuncio->imagem? 'images/' . $anuncio->imagem:'img/default-image.jpg') }}" alt="">
                    </a>
                    <div class="card-body">
                      <h4 class="card-title">
                          <a href="{{ action('HomeController@detalhes',['anuncio' => $anuncio->id]) }}">{{$anuncio->nome}}</a>
                      </h4>
                      <h5>R$ {{$anuncio->preco}}</h5>
                      <p class="card-text"><b>{{$anuncio->livro->titulo}}</b></p>
                      <p class="card-text">{{$anuncio->descricao}}</p>
                    </div>
                    
                </div>
                </div>
                @endforeach
                

            </div>
          <!-- /.row -->

        </div>
        <!-- /.col-lg-9 -->

      </div>
      <!-- /.row -->

    </div>
    <!-- /.container -->
@endsection
