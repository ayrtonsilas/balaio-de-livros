@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center text-center">
        
        <div class="col-sm-6">
            <h1>Dados Pessoais</h1>
        </div>
    </div>
    <div class="row justify-content-center text-center">
        <div class="col-sm-6">
            @if (\Session::has('success'))
                <div class="alert alert-success alert-dismissible fade show">
                    <ul>
                        <li>{!! \Session::get('success') !!}</li>
                    </ul>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-sm-6">
            <form method="post" action="{{ action('OperadorController@salvar') }}">
            {{ csrf_field() }}
            <input type="hidden" name="id" value="{{old('id',$operador->id)}}">
            <div class="form-group">
                    <label for="name">Nome:</label>
                    <input readonly name="name" type="text" class="form-control" id="name" value="{{old('name',$operador->name)}}">
                </div>
                <div class="form-group">
                    <label for="email">email:</label>
                    <input readonly name="email" type="mail" class="form-control" id="email" value="{{old('email',$operador->email)}}">
                </div>
                <div class="form-group">
                    <label for="password">Senha:</label>
                    <input name="password" type="password" class="form-control" id="password" minlength="6" value="">
                </div>
                <div class="form-group">
                    <label for="data_nasc">Data Nascimento:</label>
                    <input readonly name="data_nasc" type="date" class="form-control" id="data_nasc" value="{{old('data_nasc', $operador->data_nasc)}}">
                </div>
                <div class="form-group">
                    <label for="telefone">Telefone:</label>
                    <input name="telefone" type="text" class="form-control" id="telefone" value="{{old('telefone', $operador->telefone)}}">
                </div>
                <a href="{{ URL::previous() }}" class="btn btn-primary">Voltar</a>
                <button type="submit" class="btn btn-success">Salvar</button>
                
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
   showMateria();
</script>
@endsection