@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center text-center">
        
        <div class="col-sm-6">
            <h1>Notificações</h1>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-sm-6">
            
            @if (\Session::has('success'))
                <div class="alert alert-success alert-dismissible fade show">
                    <ul>
                        <li>{!! \Session::get('success') !!}</li>
                    </ul>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            @if (\Session::has('error'))
                <div class="alert alert-danger alert-dismissible fade show">
                    <ul>
                        <li>{!! \Session::get('error') !!}</li>
                    </ul>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            
            <a href="{{action('LivroController@novo')}}" class="btn btn-success">Novo</a><br><br>
            <table style="border:1px solid #000;" class="table table-striped">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">quem enviou</th>
                        <th scope="col">quem recebou</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach(collect([]) as $livro)
                    <tr>
                        <th scope="row">
                            <a href="{{ action('LivroController@editar',['livro' => $livro->id]) }}">
                            {{$livro->id}}
                            </a>
                        </th>
                        <td>
                            <a href="{{ action('LivroController@editar',['livro' => $livro->id]) }}">
                            {{$livro->titulo}}
                            </a>
                        </td>
                        <td>
                            <a href="{{ action('LivroController@editar',['livro' => $livro->id]) }}">
                                {{$livro->autor}}
                            </a>
                        </td>
                        <td>
                            <a href="{{ action('LivroController@editar',['livro' => $livro->id]) }}">
                                {{$livro->tipo == 'paradidatico'? "Paradidático":"Didático"}}
                            </a>
                        </td>
                        <td>
                            <a href="{{ action('LivroController@editar',['livro' => $livro->id]) }}">
                                {{$livro->edicao}}
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection