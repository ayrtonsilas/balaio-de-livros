@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center text-center">
        
        <div class="col-sm-6">
            <h1>Interesses</h1>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-sm-12">
            
            @if (\Session::has('success'))
                <div class="alert alert-success alert-dismissible fade show">
                    <ul>
                        <li>{!! \Session::get('success') !!}</li>
                    </ul>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            @if (\Session::has('error'))
                <div class="alert alert-danger alert-dismissible fade show">
                    <ul>
                        <li>{!! \Session::get('error') !!}</li>
                    </ul>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            
            <table style="border:1px solid #000;" class="table table-striped">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">Título Anúncio</th>
                        <th scope="col">Interessado</th>
                        <th scope="col">E-mail</th>
                        <th scope="col">Telefone</th>
                        <th scope="col">Status</th>
                        <th scope="col" class="text-center">Excluir</th>
                        <th scope="col" class="text-center">Concluir</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($interesses as $interesse)
                    <tr>
                        <th scope="row">
                            {{$interesse->NOMEANUNCIO}}
                        </th>
                        <td>
                            {{$interesse->NOMEINTERESSADO}}
                        </td>
                        <td>
                            {{$interesse->EMAILINTERESSADO}}
                        </td>
                        <td>
                            {{$interesse->TELEFONEINTERESSADO}}
                        </td>
                        <td>
                            {{$interesse->status}}
                        </td>
                        <td class="text-center">
                            <form action="{{ action('InteresseController@excluir',['interesse' => $interesse->id]) }}" method="POST">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                            </form>
                        </td>
                        <td class="text-center">
                            @if($interesse->status != 'Concluido')
                            <form action="{{ action('InteresseController@concluir') }}" method="POST">
                                {{ csrf_field() }}
                                <input type="hidden" name="id" value="{{$interesse->id}}">
                                <button type="submit" class="btn btn-success"><i class="far fa-check-circle"></i>
</button>
                            </form>
                            @else
                            --
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection