@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center text-center">
        
        <div class="col-sm-6">
            <h1>Livro</h1>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-sm-6">
            <form method="post" action="{{ action('LivroController@salvar') }}">
            {{ csrf_field() }}
            <input type="hidden" name="id" value="{{old('id',$livro->id)}}">
            <div class="form-group">
                    <label for="titulo">Título:</label>
                    <input name="titulo" type="text" class="form-control" id="titulo" value="{{old('titulo',$livro->titulo)}}">
                </div>
                <div class="form-group">
                    <label for="autor">Autor:</label>
                    <input name="autor" type="text" class="form-control" id="autor" value="{{old('autor',$livro->autor)}}">
                </div>
                <div class="form-group">
                    <label for="tipo">Tipo:</label>
                    <select required class="form-control" name="tipo" id="tipo" onChange="showMateria()">
                        <option value="">Selecione um tipo</option>
                        <option {{ old('tipo',$livro->tipo) == 'didatico' ? "selected":"" }} value="didatico">Didático</option>
                        <option {{ old('tipo',$livro->tipo) == 'paradidatico' ? "selected":"" }} value="paradidatico">Paradidático</option>
                    </select>
                </div>
                <div class="form-group" id="materiaArea">
                    <label for="materia">Materia:</label>
                    <select class="form-control" name="materia" id="materia">
                        <option value="">Selecione uma matéria</option>
                        <option {{ old('materia', $livro->materia) == 'portugues' ? "selected":"" }} value="portugues">Português</option>
                        <option {{ old('materia', $livro->materia) == 'matematica' ? "selected":"" }} value="matematica">Matemática</option>
                        <option {{ old('materia', $livro->materia) == 'fisica' ? "selected":"" }} value="fisica">Física</option>
                        <option {{ old('materia', $livro->materia) == 'literatura' ? "selected":"" }} value="literatura">Literatura</option>
                        <option {{ old('materia', $livro->materia) == 'historia' ? "selected":"" }} value="historia">História</option>
                        <option {{ old('materia', $livro->materia) == 'geografia' ? "selected":"" }} value="geografia">Geografia</option>
                        <option {{ old('materia', $livro->materia) == 'quimica' ? "selected":"" }} value="quimica">Química</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="edicao">Edição:</label>
                    <input name="edicao" type="text" class="form-control" id="edicao" value="{{old('edicao', $livro->edicao)}}">
                </div>
                <a href="{{ URL::previous() }}" class="btn btn-primary">Voltar</a>
                <button type="submit" class="btn btn-success">Salvar</button>
                @if($acao != "novo")
                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal">
                    Excluir
                </button>
                @endif
                
            </form>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            Tem certeza que deseja excluir o livro <b>{{$livro->titulo}}</b>?
        </div>
        <div class="modal-footer">
            <form action="{{ action('LivroController@excluir',['livro' => $livro->id]) }}" method="POST">
                {{ method_field('DELETE') }}
                {{ csrf_field() }}
                <button type="button" class="btn btn-primary" data-dismiss="modal">Cancelar</button>
                <button class="btn btn-danger" type="submit" data-toggle="modal" data-target="#confirmDelete">
                    Excluir
                </button>
            </form>
        </div>
        </div>
    </div>
</div>
<script type="text/javascript">
   showMateria();
</script>
@endsection