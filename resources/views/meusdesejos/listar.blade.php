@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center text-center">
        
        <div class="col-sm-6">
            <h1>Meus Desejos</h1>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-sm-10">
            @if (\Session::has('success'))
                <div class="alert alert-success alert-dismissible fade show">
                    <ul>
                        <li>{!! \Session::get('success') !!}</li>
                    </ul>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            @if (\Session::has('error'))
                <div class="alert alert-danger alert-dismissible fade show">
                    <ul>
                        <li>{!! \Session::get('error') !!}</li>
                    </ul>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <table style="border:1px solid #000;margin-top:50px;" class="table table-striped">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Título</th>
                        <th scope="col">Autor</th>
                        <th scope="col">Tipo</th>
                        <th scope="col">Edição</th>
                        <th scope="col">Publicado Por</th>
                        <th scope="col">Situação</th>
                        <th class="text-center" scope="col">Desistir</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($livros as $livro)
                    <tr>
                        <th scope="row">
                            
                            {{$livro->id}}
                            
                        </th>
                        <td>
                            
                            {{$livro->titulo}}
                            
                        </td>
                        <td>
                            
                                {{$livro->autor}}
                            
                        </td>
                        <td>
                            
                                {{$livro->tipo == 'paradidatico'? "Paradidático":"Didático"}}
                            
                        </td>
                        <td>
                            
                                {{$livro->edicao}}
                            
                        </td>
                        <td>
                            
                                {{$livro->publicadopor}}
                            
                        </td>
                        <td>
                            
                                {{$livro->status}}
                            
                        </td>
                        <td class="text-center">
                            @if($livro->status != 'Concluido')
                            <form action="{{ action('MeusDesejosController@desistir',['interesse' => $livro->idinteresse]) }}" method="POST">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                            </form>
                            @else
                            --
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection